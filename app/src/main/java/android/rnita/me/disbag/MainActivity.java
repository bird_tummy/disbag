package android.rnita.me.disbag;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import com.nifty.cloud.mb.NCMB;


public class MainActivity extends Activity implements View.OnClickListener {
    ImageButton diser, mrs;
    byte[] strByte;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NCMB.initialize(this, "ff2b60fe48410d5a825387a2fb116e0d3f791414cafb908adf980b065cd72b8f", "43fe8e6af8b7c016cbd3ab26f3e7bd3c7ef3a8ebf50764e6496427c2c06ce441");

        // ディーサー画面ボタン
        diser = (ImageButton) findViewById(R.id.diser);
        diser.setOnClickListener(this);
        // 伝説のお母さん画面ボタン
        mrs = (ImageButton) findViewById(R.id.mrs);
        mrs.setOnClickListener(this);

        settoolbar();

    }

    public void onClick(View v) {
        if (v == diser) {
            Intent intent = new Intent();
            intent.setClass(this, diserListActivity.class);
            startActivity(intent);
        } else if (v == mrs) {
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, MrsList.class);
            startActivity(intent);

        }

    }

    protected void settoolbar() {
        //  toolbar定義
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        //  logoは画像を表示するのみ
        toolbar.setLogo(R.mipmap.icon48);

    }

}
