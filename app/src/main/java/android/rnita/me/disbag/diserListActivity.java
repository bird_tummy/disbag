package android.rnita.me.disbag;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nifty.cloud.mb.GetDataCallback;
import com.nifty.cloud.mb.NCMB;
import com.nifty.cloud.mb.NCMBException;
import com.nifty.cloud.mb.NCMBFile;
import com.nifty.cloud.mb.SaveCallback;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


public class diserListActivity extends ListActivity implements View.OnClickListener {
    private ImageButton postButton;
    private SharedPreferences pref;
    private ArrayList<ArrayList<String>> arrayLists;
    private View view;
    private DisContentsAdapter adapter;
    private byte[] strByte;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diser_list);

        postButton = (ImageButton) findViewById(R.id.post_button);
        postButton.setOnClickListener(this);

        NCMB.initialize(this, "ff2b60fe48410d5a825387a2fb116e0d3f791414cafb908adf980b065cd72b8f", "43fe8e6af8b7c016cbd3ab26f3e7bd3c7ef3a8ebf50764e6496427c2c06ce441");

        NCMBFile fileData = new NCMBFile("dis.json");
        fileData.getDataInBackground(new GetDataCallback() {

            @Override
            public void done(byte[] obj, NCMBException e) {
                // TODO Auto-generated method stub
                if (e != null) {
                    Log.d("MrsList", "error in getdata");
                } else {
                    // TODO Auto-generated method stub
                    String jsonStr;
                    try {
                        jsonStr = new String(obj, "UTF-8");
                        Gson gson = new Gson();
                        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
                        History history = gson.fromJson(jsonStr, History.class);
                        arrayLists = history.getDis();

                        history.setDis(arrayLists);
                        pref.edit().putString("history", gson.toJson(history)).apply();

                        if (arrayLists.size() == 0) {
                            Toast.makeText(diserListActivity.this, "データがありません", Toast.LENGTH_SHORT).show();
                        } else {

                            adapter = new DisContentsAdapter(diserListActivity.this);
                            adapter.setList(arrayLists);

                            ListView listView = getListView();
                            listView.setAdapter(adapter);
                        }
                    } catch (UnsupportedEncodingException exception) {
                        exception.printStackTrace();
                    }


                }
            }

        });
        settoolbar();
    }

    @Override
    protected void onListItemClick(ListView listView, View v, int position, long id) {
        super.onListItemClick(listView, v, position, id);

        if (arrayLists.get(position).size() == 1) {
            Toast.makeText(diserListActivity.this, "このdisに対するカーチャンからの助言はありません…", Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(diserListActivity.this, mrsResultActivity.class);
            intent.putExtra("position", position);
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.post_button) {
            LayoutInflater layoutInflater = LayoutInflater.from(this);
            view = layoutInflater.inflate(R.layout.dialog, null);
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getResources().getString(R.string.dialog_title));
            builder.setView(view);

            final EditText dis_edit = (EditText) view.findViewById(R.id.dis_edittext);

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    ArrayList<String> addList = new ArrayList<>();
                    addList.add(dis_edit.getText().toString());
                    arrayLists.add(addList);
                    History history = new History();
                    history.setDis(arrayLists);
                    Gson gson = new Gson();

                    String str = gson.toJson(history);
                    try {
                        strByte = str.getBytes("UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    NCMBFile file = new NCMBFile("dis.json", strByte);
                    file.saveInBackground
                            (new SaveCallback() {
                                @Override
                                public void done(NCMBException e) {
                                    adapter.notifyDataSetChanged();
                                    Toast.makeText(diserListActivity.this, dis_edit.getText().toString() + " を追加しました", Toast.LENGTH_LONG).show();
                                }
                            });
                    finish();
                }
            });

            builder.setNegativeButton("キャンセル", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    protected void settoolbar() {
        //  toolbar定義
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        //  logoは画像を表示するのみ
        toolbar.setLogo(R.mipmap.icon48);
        toolbar.setTitle(" dis を選んでね！");
    }

}
