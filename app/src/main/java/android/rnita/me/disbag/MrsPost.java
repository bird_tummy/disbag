package android.rnita.me.disbag;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nifty.cloud.mb.NCMBException;
import com.nifty.cloud.mb.NCMBFile;
import com.nifty.cloud.mb.SaveCallback;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


public class MrsPost extends Activity implements View.OnClickListener {
    ImageButton postbtn;
    EditText postdata_edit;
    int pos;
    String postName;
    ArrayList<ArrayList<String>> arrayLists;
    byte[] strByte;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrs_post);


        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        String objectId = pref.getString("objectID", "");
        Gson gson = new Gson();
        History history = gson.fromJson(pref.getString("history", ""), History.class);
        arrayLists = history.getDis();

        postdata_edit = (EditText) findViewById(R.id.postdata);

        //前画面でのListViewのポジションを取得
        Intent position = getIntent();
        pos = position.getIntExtra("position", 0);

        //ListViewで取得したボタンの位置で、niftyのIdを呼び出す
        postName = objectId;

        //投稿ボタン
        postbtn = (ImageButton) findViewById(R.id.post);
        postbtn.setOnClickListener(this);

        settoolbar();
    }

    //        -----------------ここから修正前のコード-----------------
    public void onClick(View v) {
        if (v == postbtn) {
            SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
            History history = new History();
            Intent intent = getIntent();
            int pos = intent.getIntExtra("position", 0);

            // 新たなコメントを追加
            if (postdata_edit.getText().toString() != null) {
                ArrayList<String> arrayList = arrayLists.get(pos);
                arrayList.add(postdata_edit.getText().toString());
                arrayLists.remove(pos);
                arrayLists.add(pos, arrayList);

                Gson gson = new Gson();
                history.setDis(arrayLists);
                String str = gson.toJson(history);
                try {
                    strByte = str.getBytes("UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                NCMBFile file = new NCMBFile("dis.json", strByte);
                file.saveInBackground
                        (new SaveCallback() {
                            @Override
                            public void done(NCMBException e) {
                                Toast.makeText(MrsPost.this, "コメントを追加しました", Toast.LENGTH_SHORT).show();
                                Intent home = new Intent();
                                home.setClass(MrsPost.this, MainActivity.class);
                                startActivity(home);
                                finish();
                                postdata_edit.setText("");
                            }
                        });
            } else {
                Toast.makeText(MrsPost.this, "コメントを入力してください", Toast.LENGTH_SHORT).show();
            }

        }
    }

    protected void settoolbar(){
        //  toolbar定義
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        //  logoは画像を表示するのみ
        toolbar.setLogo(R.mipmap.icon48);
    }
}
