package android.rnita.me.disbag;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.google.gson.Gson;

import java.util.ArrayList;


public class mrsResultActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrs_result);

        Intent intent = getIntent();
        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        Gson gson = new Gson();
        ImageButton endButton = (ImageButton) findViewById(R.id.end_button);
        endButton.setOnClickListener(this);

        History history = gson.fromJson(pref.getString("history", ""), History.class);
        ArrayList<ArrayList<String>> arrayLists = history.getDis();
        ArrayList<String> list = arrayLists.get(intent.getIntExtra("position", 0));
        list.remove(0);     // 0番目はdisなので削除
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner_textview, list);
        adapter.setDropDownViewResource(R.layout.spinner_textview);

        Spinner spinner = (Spinner) findViewById(R.id.mrs_result);
        spinner.setAdapter(adapter);

        settoolbar();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.end_button) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    protected void settoolbar(){
        //  toolbar定義
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        //  logoは画像を表示するのみ
        toolbar.setLogo(R.mipmap.icon48);
    }
}
