package android.rnita.me.disbag;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

public class DisContentsAdapter extends BaseAdapter {

    LayoutInflater layoutInflater = null;
    Context context;

    @Getter
    @Setter
    ArrayList<ArrayList<String>> disList;

    public DisContentsAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setList(ArrayList<ArrayList<String>> disList) {
        this.disList = disList;
    }

    public int getCount() {
        return disList.size();
    }

    public Object getItem(int position) {
        return disList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.list_layout, parent, false);

        ((TextView) convertView.findViewById(R.id.dis_textview)).setText(disList.get(position).get(0));
        ((TextView) convertView.findViewById(R.id.count_textview))
                .setText(Integer.toString(disList.get(position).size() - 1));

        return convertView;
    }

}
