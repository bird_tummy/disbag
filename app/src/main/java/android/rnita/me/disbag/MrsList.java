package android.rnita.me.disbag;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nifty.cloud.mb.GetDataCallback;
import com.nifty.cloud.mb.NCMB;
import com.nifty.cloud.mb.NCMBException;
import com.nifty.cloud.mb.NCMBFile;
import com.nifty.cloud.mb.NCMBObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


public class MrsList extends Activity {
    ArrayAdapter<String> adapter;
    ArrayList<ArrayList<String>> arrayLists;
    NCMBObject obj;
    String resultobj;
    String jsonStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrs_list);

        NCMB.initialize(this, "ff2b60fe48410d5a825387a2fb116e0d3f791414cafb908adf980b065cd72b8f", "43fe8e6af8b7c016cbd3ab26f3e7bd3c7ef3a8ebf50764e6496427c2c06ce441");

        NCMBFile fileData = new NCMBFile("dis.json");
        fileData.getDataInBackground(new GetDataCallback() {

            @Override
            public void done(byte[] obj, NCMBException e) {
                // TODO Auto-generated method stub
                if (e != null) {
                    Log.d("MrsList", "error in getdata");
                } else {
                    // TODO Auto-generated method stub
                    String jsonStr;
                    try {
                        jsonStr = new String(obj, "UTF-8");
                        Gson gson = new Gson();
                        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
                        History history = gson.fromJson(jsonStr, History.class);
                        arrayLists = history.getDis();

                        history.setDis(arrayLists);
                        pref.edit().putString("history", gson.toJson(history)).apply();

                        if (arrayLists.size() == 0) {
                            Toast.makeText(MrsList.this, "データがありません", Toast.LENGTH_SHORT).show();
                        } else {
                            ArrayList<String> arrayList = new ArrayList<>();
                            for (int i = 0; i < arrayLists.size(); i++) {
                                arrayList.add(arrayLists.get(i).get(0));
                            }

                            ListView listView = (ListView) findViewById(R.id.listView);
                            adapter = new ArrayAdapter(MrsList.this, R.layout.list_layout_mrs, arrayList);
                            listView.setAdapter(adapter);
                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                                    Intent intent = new Intent();
                                    intent.putExtra("position", pos);
                                    intent.setClass(MrsList.this, MrsPost.class);
                                    startActivity(intent);
                                }
                            });

                        }
                    } catch (UnsupportedEncodingException exception) {
                        exception.printStackTrace();
                    }


                }
            }

        });




//        -----------------ここから修正前のコード-----------------

//        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
//        Gson gson = new Gson();
//        History history = gson.fromJson(pref.getString("history", ""), History.class);
//
//        final ArrayList<ArrayList<String>> arrayLists = history.getDis();
//        ArrayList<String> dis0 = history.getCategory();
//
//        adapter = new ArrayAdapter<>(this, R.layout.list_layout);
//        for (int i = 0; i < arrayLists.size(); i++){
//            adapter.add(arrayLists.get(i).get(0));
//        }

//        -----------------ここまで修正前のコード-----------------

        settoolbar();
    }

    protected void settoolbar(){
        //  toolbar定義
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        //  logoは画像を表示するのみ
        toolbar.setLogo(R.mipmap.icon48);
        toolbar.setTitle(" どのdisに助言する？");
    }

}
